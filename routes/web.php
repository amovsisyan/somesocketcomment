<?php


Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::group(['middleware' => 'web','auth'], function () {

    Route::get('/home', 'HomeController@index');

    Route::get('/addpost', ['as'=>'add_post_get','uses'=>'PostController@index']);
    Route::post('/addpost', ['as'=>'add_post_post','uses'=>'PostController@addPost']);

    Route::get('/allposts', ['as'=>'allposts_get','uses'=>'PostController@allPosts']);
    Route::get('/allposts/{id}', ['as'=>'current_post_get','uses'=>'PostController@currentPost'])->where('id','^[1-9][0-9]*$');
    Route::put('/allposts/{id}', ['as'=>'current_post_put','uses'=>'CommentController@addComment'])->where('id','^[1-9][0-9]*$');

    Route::post('/changepost', ['as'=>'changepost','uses'=>'PostController@changePost']);
    Route::delete('/deletepost', ['as'=>'deletepost','uses'=>'PostController@deletePost']);

});
