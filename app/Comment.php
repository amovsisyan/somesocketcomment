<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Comment extends Model
{
    protected $fillable = [
        'comment_content', 'created_by'
    ];

    public function user()
    {
        return $this->belongsTo('App\User','comments.created_by','users.id');
    }

    public function post()
    {
        return $this->belongsTo('App\Post','for_post','posts.post_id');
    }
}
