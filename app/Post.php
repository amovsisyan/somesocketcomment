<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Post extends Model
{
    protected $primaryKey = 'post_id';

    protected $fillable = [
        'post_name', 'post_content', 'post_avatar',
    ];

    protected $hidden = [
        'updated_at'
    ];

    public function user()
    {
        return $this->belongsTo('App\User','created_by','users.id');
    }

    public function comments()
    {
        return $this->hasMany('App\Comment','for_post','post_id');
    }
}
