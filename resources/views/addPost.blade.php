@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-xs-8 col-xs-offset-2">
                <form method="post" class="adding" action="" enctype="multipart/form-data">
                    <div class="form-group {{ $errors->has('postName') ? ' has-error' : '' }}">
                        <label>Your Post Name</label>
                        <input type="text" class="form-control" id="postName" placeholder="Post Name" name="postName">
                        @if ($errors->has('postName'))
                            <span class="help-block">
                                <strong>{{ $errors->first('postName') }}</strong>
                            </span>
                        @endif
                    </div>
                    <div class="form-group {{ $errors->has('postContent') ? ' has-error' : '' }}">
                        <label>Your Post Content</label>
                        <input type="text" class="form-control" id="postContent" placeholder="Post Content" name="postContent">
                        @if ($errors->has('postContent'))
                            <span class="help-block">
                                <strong>{{ $errors->first('postContent') }}</strong>
                            </span>
                        @endif
                    </div>
                    <div class="form-group {{ $errors->has('file') ? ' has-error' : '' }}">
                        <label>Post IMG</label>
                        <input type="file" class="form-control" id="file" name="file">
                        @if ($errors->has('file'))
                            <span class="help-block">
                                <strong>{{ $errors->first('file') }}</strong>
                            </span>
                        @endif
                    </div>
                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                    <button type="submit" id="add_post" class="btn btn-default">Submit</button>
                </form>
            </div>
        </div>
    </div>
    <script>
        $(document).on('submit','form.adding',function(e){
            $button = $('#add_post');
            $parent = $button.closest( "div" );
            $button.remove();
            $parent.append('<div><img class="after_add" src="/img/forapp/load.gif"></div>')
        });
    </script>
@endsection
