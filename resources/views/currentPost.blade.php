@extends('layouts.app')


@section('content')
    <div class="container">
        <div class="row">
            <div class="col-xs-12">
                <div class="col-xs-12 getter" id ="{{$current_post->post_id}}">
                    <div class="outline">
                        <div class="col-xs-4 for_post_img">
                            <img src="/img/posts/{{ $current_post->post_avatar }}" alt="">
                        </div>
                        <div class="col-xs-8">
                            <div class="info">
                                <p id="post_name">{{$current_post->post_name}}</p>
                                <p id="post_content">{{$current_post->post_content}}</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="container">
        <div class="row">
            <div class="col-xs-12" id = "writingArea">
                <div class="form-group">
                    <label for="comment">Comment:</label>
                    <textarea class="form-control comment-control" rows="3" id="comment"></textarea>
                    <div class="btn btn-success" id = "btn_send_comment">Commit</div>
                    <input type="hidden" name="_id" id="auth_id" value="{{$auth->id}}">
                    <input type="hidden" name="_name" id="auth_name" value="{{$auth->name}}">
                </div>
            </div>
        </div>
    </div>
    <div class="container">
        <div class="row needed">
            @foreach ($comments as $comment)
                <div class="col-xs-12">
                    <div class="panel panel-white post panel-shadow">
                        <div class="post-heading">
                            <div class="pull-left meta">
                                <div class="title h5">
                                    <a href="#"><b>{{ $comment->name }}</b></a>
                                </div>
                                <h6 class="text-muted time">made a post on. {{ $comment->created_at }}</h6>
                            </div>
                        </div>
                        <div class="post-description">
                            <p>{{ $comment->comment_content }}</p>
                        </div>
                    </div>
                </div>
            @endforeach
        </div>
    </div>
@endsection
