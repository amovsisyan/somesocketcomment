@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-xs-12">
                @foreach($all_posts as $post)
                    <div class="col-xs-12 getter" id="{{$post->post_id}}">
                        <div class="outline">
                            @if (Auth::id()==$post->created_by)
                                <div>
                                    <button class="btn btn-danger btn-sm post_but post_del">X</button>
                                </div>
                                <div>
                                    <button class="btn btn-warning btn-sm post_but post_edit">ed</button>
                                </div>
                            @endif
                            <div class="col-xs-4 for_post_img">
                                <img src="/img/posts/{{ $post->post_avatar }}" alt="">
                            </div>
                            <div class="col-xs-8">
                                <div class="info">
                                    <p id="post_name">{{$post->post_name}}</p>
                                    <p id="post_content">{{$post->post_content}}</p>
                                </div>
                            </div>
                            <div class="see_more">
                                <a href="/allposts/{{ $post->post_id }}">
                                    <span>See More -></span>
                                </a>
                            </div>
                        </div>
                    </div>
                @endforeach
            </div>
            {{ $all_posts->links() }}
        </div>
    </div>

    {{--        Editing Pop_Up      --}}
    <div class="col-xs-8 col-xs-offset-2 pop_up">
        <div class="form-group">
            <label>Your Post Name</label>
            <input type="text" class="form-control" id="pp_post_name" placeholder="Post Name" name="postName">
        </div>
        <div class="form-group">
            <label>Your Post Content</label>
            <input type="text" class="form-control" id="pp_post_content" placeholder="Post Content" name="postContent">
        </div>
        <div class="form-group">
            <label>Post IMG</label>
            <input type="file" class="form-control" id="pp_file" name="file">
        </div>
        <div class="form-group">
            <input id="pp_post_id" type="hidden" value="">
        </div>
        <input type="hidden" name="_token" value="{{ csrf_token() }}">
        <span>
            <button id="save_change" class="btn btn-success">Save Changes</button>
            <button id="cancel" class="btn btn-default">Cancel</button>
        </span>
    </div>

    <script>
        $(document).ready(function () {
            $(document).on("click",".post_edit",function() {
                var id = $(this).parents('.getter').attr('id');
                $post_name = $('#'+id).find("#post_name").html();
                $post_content = $('#'+id).find("#post_content").html();
                $('#pp_post_name').val($post_name);
                $('#pp_post_content').val($post_content);
                $('#pp_post_id').val(id);
                $('.pop_up').fadeIn();
            });

            $(document).on("click","#cancel",function() {
                $('.pop_up').fadeOut();
            });

            $(document).on("click","#save_change",function() {
                $pop_up = $('.pop_up');
                $button_save = $('#save_change');
                $button_canc = $('#cancel');
                $parent = $button_save.closest( "span" );
                $button_save.remove();
                $button_canc.remove();
                $parent.prepend('<img class="after_add" src="/img/forapp/load.gif">')
                var post_name = $('#pp_post_name').val();
                var post_content = $('#pp_post_content').val();
                var id_post = $('#pp_post_id').val();
                var file2 = document.getElementById("pp_file").files[0];

                var formData = new FormData();
                formData.append('post_name',post_name);
                formData.append('post_content',post_content);
                formData.append('id_post',id_post);
                formData.append('img',file2);
                $.ajax({
                    type:'post',
                    url:'/changepost',
                    cache: false,
                    enctype: 'multipart/form-data',
                    data:formData,
                    dataType: "json",
                    processData: false,
                    contentType: false,
                    headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') },
                    success:function(data){
                        if (data['error']) {
                            $('.pop_up .validation_error').remove();
                            $pop_up.append('<p class="validation_error"> The input is not valid </p>');
                            $('.after_add').remove();
                            $parent.append('<button id="save_change" class="btn btn-success">Save Changes</button>');
                            $parent.append('<button id="cancel" class="btn btn-default">Cancel</button>');
                            return;
                        }
                        var id = data['post_id'];
                        $('#'+id).find('#post_name').html(data['post_name']);
                        $('#'+id).find('#post_content').html(data['post_content']);
                        if (data['post_avatar']) {
                            $('#'+data['post_id']).find('img').fadeOut('slow', function(){
                                var thisOne = $('#'+data['post_id']).find('img');
                                thisOne.attr('src','/img/posts/'+data['post_avatar'])
                                thisOne.fadeIn('slow');
                            })
                        }
                        $pop_up.fadeOut('slow', function() {
                            $('.after_add').remove();
                            $parent.append('<button id="save_change" class="btn btn-success">Save Changes</button>');
                            $parent.append('<button id="cancel" class="btn btn-default">Cancel</button>');
                        });
                    }
                })
            });

            $(document).on("click",".post_del",function() {
                $('.pop_up').fadeOut();
                var id = $(this).parents('.getter').attr('id');
                $(this).closest('div').html('<div class="btn btn-sm post_but"><img class="after_del" src="/img/forapp/load.gif"></div>');
                $.ajax({
                    type:'delete',
                    url:'/deletepost',
                    dataType: "json",
                    headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') },
                    data:{
                        id:id
                    },
                    success:function(data) {
                        if (data.error) {
                            alert('The input is not valid'); /* Didn't want waste a time for this*/
                            return;
                        }
                        var dataId = data['post_id'];
                        $('#'+dataId).fadeOut('slow', function() {
                            $('#'+dataId).remove();
                        })
                    }
                })
            });

        });
    </script>
@endsection
